# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
mkdir stroboskop
git clone https://ins0mni4c7@bitbucket.org/ins0mni4c7/stroboskop.git
cd stroboskop
git add .
```

Naloga 6.2.3:
https://bitbucket.org/ins0mni4c7/stroboskop/commits/3db5ec0727b35f67e2857d76b691b3fb953c3beb

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ins0mni4c7/stroboskop/commits/35df4a41b195d316b1235b71ff709323d7cdf93f

Naloga 6.3.2:
https://bitbucket.org/ins0mni4c7/stroboskop/commits/82128175d6598d0f59e18e340cf0c1a3600508ee

Naloga 6.3.3:
https://bitbucket.org/ins0mni4c7/stroboskop/commits/2cffb4d1570e91b9855823f89c5dd39e42cb89d9

Naloga 6.3.4:
https://bitbucket.org/ins0mni4c7/stroboskop/commits/e2542aa89e57feccc27af9db40841413bc84ddeb

Naloga 6.3.5:

```
git add .
git commit -m "Dodajanje robov in senc"
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))

Naloga 6.4.2:
((UVELJAVITEV))

Naloga 6.4.3:
((UVELJAVITEV))

Naloga 6.4.4:
((UVELJAVITEV))